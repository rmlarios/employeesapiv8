This Repository contains ASP.Net Core 3.1 Web API used as Backend of the Test.
Implement Domain Drive Design as software development philosophy centered around the domain.
Use SQL Server as relational database management system.
The connection string used is:
Data Source=localhost;Initial Catalog=V8Employees;Integrated Security=True;MultipleActiveResultSets=True

When you run for first time the Database will be created and seed with sample values for all tables.
When debbuging use the following URL:
https:\\localhost:5001
The default page displayed its Swagger.