﻿using Domain.Base;
using Domain.Entities.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Salarys
{
    public partial class Salary:BaseEntity
    {

        public Salary()
        {

        }
       
        public Guid EmployeeId { get; private set; }
        public int Year { get; private set; }
        public int Month { get; private set; }
        public double BaseSalary { get; private set; }
        public double ProductionBonus { get; private set; }
        public double CompesationBonus { get; private set; }
        public double Comission { get; private set; }
        public double Contributions { get; private set; }
        public double OtherIncome { get; private set; }
        public double TotalSalary { get; private set; }
        public DateTime StartDate { get; set; }

        public virtual Employee Employee { get; private set; }


        

    }
}
