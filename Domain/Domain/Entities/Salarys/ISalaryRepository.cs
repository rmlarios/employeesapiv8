using System;
using Domain.Interfaces;

namespace Domain.Entities.Salarys
{
    public interface ISalaryRepository : IAsyncRepository<Salary>
    {
    }
}
