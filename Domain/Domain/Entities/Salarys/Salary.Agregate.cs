using System;
using Domain.Base;

namespace Domain.Entities.Salarys
{
  public partial class Salary : IAggregateRoot
  {
    public Salary(Guid employeeId, int year, int month, double baseSalary, double productionBonus, double compesationBonus, double comission, double contributions,DateTime startDate)
    {
      this.Update(employeeId, year, month, baseSalary, productionBonus, compesationBonus, comission, contributions,StartDate);
    }

    public void Update(Guid employeeId, int year, int month, double baseSalary, double productionBonus, double compesationBonus, double comission, double contributions,DateTime startDate)
    {
      EmployeeId = employeeId;
      Year = year;
      Month = month;
      BaseSalary = baseSalary;
      ProductionBonus = productionBonus;
      CompesationBonus = compesationBonus;
      Comission = comission;
      Contributions = contributions;
      OtherIncome = (baseSalary + comission) * 0.08 + comission;
      TotalSalary = baseSalary + productionBonus + (compesationBonus * 0.75) + OtherIncome - contributions;
      StartDate = startDate;

    }


  }
}
