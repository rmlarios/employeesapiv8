﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Divisions
{
    public partial class Division: BaseEntity
    {
        public string Name { get; private set; }
        public Division()
        {

        }
        public Division(string name)
        {
            Name = name;
        }
    }
}
