﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Employees
{
    public interface IEmployeeRepository : IAsyncRepository<Employee>
    {
    }
}
