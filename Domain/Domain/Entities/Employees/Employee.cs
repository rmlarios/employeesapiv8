﻿using Domain.Base;
using Domain.Entities.Divisions;
using Domain.Entities.Offices;
using Domain.Entities.Positions;
using Domain.Entities.Salarys;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Employees
{
    public partial class Employee : BaseEntity
    {
        public Employee()
        {
            Salarys = new HashSet<Salary>();
        }

        public string EmployeeCode { get; private set; }
        public string EmployeeName { get; private set; }
        public string EmployeeSurname { get; private set; }
        public int Grade { get; private set; }
        public DateTime Bithday { get; private set; }

        public Guid OfficeId { get; private set; }
        public Guid DivisionId { get; private set; }
        public Guid PositionId { get; private set; }
        public string IdentificationNumber { get; private set; }

        public virtual Office Office { get; private set; }
        public virtual Division Division { get; private set; }
        public virtual Position Position { get; private set; }

        public virtual ICollection<Salary> Salarys { get; private set; }



    }
}
