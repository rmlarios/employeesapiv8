﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Domain.Entities.Salarys;

namespace Domain.Entities.Employees
{
    public partial class Employee : IAggregateRoot
    {
        public Employee(string code, string name, string surname, int grade, DateTime birthdate, string identificationNumber,Guid officeId, Guid divisionId, Guid positionId,Guid? id=null)
        {
         //   EmployeeName = name;            
            this.Update(code, name, surname, grade, birthdate,identificationNumber,officeId,divisionId,positionId);
        }

        public void Update(string code, string name, string surname, int grade, DateTime birthdate, string identificationNumber, Guid officeId, Guid divisionId, Guid positionId, Guid? id = null)
        {       


            EmployeeCode = code;
            EmployeeName = name;
            EmployeeSurname = surname;
            Grade = grade;
            Bithday = birthdate;
            IdentificationNumber = identificationNumber;
            OfficeId = officeId;
            DivisionId = divisionId;
            PositionId = positionId;
        }

        /*public Salary AddSalary(int year, int month, double baseSalary, double productionBonus, double compesationBonus, double comission, double contributions,DateTime startDate)
        {
            //Validate there's only a salary per Month/Year
            var exist = this.Salarys.Any(x => x.Year == year && x.Month == month && x.EmployeeId == this.Id);
            if (exist)
                throw new Exception("Salary for this Month already exist.");

            var salary = new Salary(this.Id, year, month, baseSalary, productionBonus, compesationBonus, comission, contributions,startDate);
            this.Salarys.Add(salary);
            return salary;
        }*/
    }
}
