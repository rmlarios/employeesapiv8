﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Positions
{
    public partial class Position: BaseEntity
    {
        public string Name { get; private set; }
        public Position()
        {

        }
        public Position(string name)
        {
            Name = name;
        }
    }
}
