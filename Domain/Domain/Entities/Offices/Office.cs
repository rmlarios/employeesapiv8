﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities.Offices
{
    public partial class Office : BaseEntity
    {
        public string Name { get; private set; }

        public Office()
        {

        }

        public Office(string name)
        {
            Name = name;
            this.Update(name);
        }

        public void Update(string name)
        {
            Name = name;
        }
    }
}
