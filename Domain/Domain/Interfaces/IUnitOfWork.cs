﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesdAsync();
        IAsyncRepository<T> AsyncRepository<T>() where T : BaseEntity;

    }
}
