﻿using Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IAsyncRepository<T> where T : BaseEntity
    {
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task<bool> DeleteAsync(T entity);
        Task<T> GetAsync(Expression<Func<T,bool>> expression, string Include="");
        Task<List<T>> ListAsync(Expression<Func<T, bool>> expression=null, string Include="",int pageIndex=0,int pageSize=0);

        Task<List<M>> ListAll<M>() where M : class;

         Task<int> GetCount<M>(Expression<Func<M, bool>> expression = null) where M : class;

        


  }
}
