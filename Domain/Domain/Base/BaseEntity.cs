﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Base
{
    public abstract class BaseEntity
    {
        public Guid Id { get; private set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }

        protected BaseEntity()
        {
            CreatedOn = DateTime.UtcNow;
            LastModifiedOn = DateTime.UtcNow;
            Id = NewId.Next().ToGuid();
        }
    }
}
