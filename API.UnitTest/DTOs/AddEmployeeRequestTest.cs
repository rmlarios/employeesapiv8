using API.DTOs.Employees;
using API.UnitTest.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace API.UnitTest.Entities
{
    public class AddEmployeeRequestTest : BaseTest
    {
        const string validGuid = "C56A4180-65AA-42EC-A945-5FD21DEC0538";
        const string validDate = "2017-3-1";

        [Theory]
        [InlineData("Name", "Surname", "1111111111", "1111111111", 1, validDate, validGuid, validGuid, validGuid, 0)]
        [InlineData(null, "Surname", "7777777", "999999999", -1,validDate,validGuid,validGuid,validGuid,4 )]
        [InlineData("Name", null, "7777777", "999999999", 1,null,validGuid,validGuid,validGuid,3 )]                
        public void ValidateModel_AddEmployeeRequest_ReturnCorrectNumberofErrors(string _name,string _surname, string _code, string _identificationnumber,int _grade,string _birthday,Guid _officeId,Guid _divisionId, Guid _positionId,int expectedErrors)
        {
            var request = new AddEmployeeRequest
            {
                employeeName = _name,
                employeeSurname= _surname,
                employeeCode=_code,
                identificationNumber=_identificationnumber,
                grade=_grade,
                birthday=Convert.ToDateTime(_birthday),
                officeId=_officeId,
                divisionId=_divisionId,
                positionId=_positionId                
            };

            var errorList = ValidateModel(request);

            Assert.Equal(expectedErrors, errorList.Count);
        } 
    }
}
