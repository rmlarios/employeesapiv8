using System;
using System.Linq;
using System.Threading.Tasks;
using API.DTOs.Employees;
using API.Services.Employees;
using API.Services.Salaries;
using Domain.Entities.Salarys;
using Infraestructure.Data;
using Xunit;

namespace API.UnitTest.Services
{
  public class SalaryServiceTests: IClassFixture<SqliteInMemoryTest>
  {
    private SqliteInMemoryTest _fixture { get; }
    public SalaryServiceTests(SqliteInMemoryTest fixture)
    {
      _fixture = fixture;
    }



    [Fact]
    public void Add_SalaryWithSameMonthAndYear_ThrowsException()
    {
      using (var context = _fixture.CreateContext())
      {
        var uOw = new UnitOfWork(context);        
        var service = new SalaryService(uOw);
        Salary existingSalary= context.Salaries.FirstOrDefault();
        AddSalaryRequest newSalary = new AddSalaryRequest()
        {
          EmployeeId = existingSalary.EmployeeId,
          Year= existingSalary.Year,
          Month= existingSalary.Month,
          BaseSalary = new Random().Next(1,5000),
          ProductionBonus = new Random().Next(1,5000),
          CompesationBonus = new Random().Next(1,1000),
          Comission = new Random().Next(1,500),
          Contributions = new Random().Next(1,500)
      };

        Assert.ThrowsAsync<Exception>(() => service.AddEmployeeSalaryAsync(newSalary));
      }
    }
  }
}
