﻿using API.DTOs.Employees;
using API.Services.Employees;
using Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace API.UnitTest.Services
{
  public class EmployeeServiceTests : IClassFixture<SqliteInMemoryTest>
  {
    private SqliteInMemoryTest _fixture { get; }
    public EmployeeServiceTests(SqliteInMemoryTest fixture)
    {
      _fixture = fixture;
    }
    [Fact]
    public void AddUpdateEmployee_DuplicatedFullName_ThrowsException()
    {
      using (var context = _fixture.CreateContext())
      {
        var uOw = new UnitOfWork(context);
        var service = new EmployeeService(uOw);
        var officeId = context.Offices.FirstOrDefault().Id;
        var divisionId = context.Divisions.FirstOrDefault().Id;
        var positionId = context.Positions.FirstOrDefault().Id;

        var newEmployee = new AddEmployeeRequest()
        {
          employeeCode = "9876543210",
          employeeName = "Employee 1",
          employeeSurname = "SurName 1",
          grade = 25,
          birthday = DateTime.Now,
          identificationNumber = "0123456789",
          officeId = officeId,
          divisionId = divisionId,
          positionId = positionId
        };
        Assert.ThrowsAsync<Exception>(() => service.AddNewAsync(newEmployee));
      }
    }


    [Fact]
    public void Search_NotExistingEmployeeByCode_ThrowsException()
    {
      using (var context = _fixture.CreateContext())
      {
        var uOw = new UnitOfWork(context);
        var service = new EmployeeService(uOw);
        GetEmployeeRequest request = new GetEmployeeRequest() { code = "5555" };
        Assert.ThrowsAsync<Exception>(() => service.SearchAsync(request));
      }
    }



  }
}
