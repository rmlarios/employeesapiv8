﻿using Bogus;
using Domain.Entities.Divisions;
using Domain.Entities.Employees;
using Domain.Entities.Offices;
using Domain.Entities.Positions;
using Domain.Entities.Salarys;
using Infraestructure.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace API.UnitTest
{
    public class SqliteInMemoryTest : IDisposable
    {
        
            private static readonly object _lock = new object();
            private static bool _databaseInitialized;

            private string dbName = "TestDatabase.db";

            public SqliteInMemoryTest()
            {
                Connection = new SqliteConnection($"Filename={dbName}");

                Seed();

                Connection.Open();
            }

            public DbConnection Connection { get; }
            #nullable enable
            public DBContext CreateContext(DbTransaction? transaction = null)
            {
                var context = new DBContext(new DbContextOptionsBuilder<DBContext>().UseSqlite(Connection).Options);

                if (transaction != null)
                {
                    context.Database.UseTransaction(transaction);
                }

                return context;
            }

            private void Seed()
            {
                lock (_lock)
                {
                    if (!_databaseInitialized)
                    {
                        using (var context = CreateContext())
                        {
                            context.Database.EnsureDeleted();
                            context.Database.EnsureCreated();

                            SeedData(context);
                        }

                        _databaseInitialized = true;
                    }
                }
            }

            private void SeedData(DBContext context)
            {
                var count = 1;
                var fakeOffices = new Faker<Office>()
                    .RuleFor(o => o.Name, f => $"Office {count}");

                var offices = fakeOffices.Generate(15);
                context.AddRange(offices);
                context.SaveChanges();

                count = 1;
                var fakeDivisions = new Faker<Division>()
                   .RuleFor(o => o.Name, f => $"Division {count}");

                var divisions = fakeDivisions.Generate(10);
                context.AddRange(divisions);
                context.SaveChanges();

                count = 1;
                var fakePositions = new Faker<Position>()
                   .RuleFor(o => o.Name, f => $"Position {count}");

                var positions = fakePositions.Generate(20);
                context.AddRange(positions);
                context.SaveChanges();

                count = 1;
                DateTime startDate = new DateTime(1970, 1, 1);
                var fakeEmployees = new Faker<Employee>()
                    .RuleFor(o => o.EmployeeName, f => $"Employee {count}")
                    .RuleFor(o => o.EmployeeSurname, f => $"SurName {count}")
                    .RuleFor(o => o.EmployeeCode, f => new Random().Next().ToString("D10"))
                    .RuleFor(o => o.IdentificationNumber, f => new Random().Next().ToString("D10"))
                    .RuleFor(o => o.Bithday, f => startDate.AddDays(new Random().Next((DateTime.Today - startDate).Days)))
                    .RuleFor(o => o.Grade, f => new Random().Next(0, 50))
                    //.RuleFor(o => o.OfficeId, ()=> context.Offices.OrderBy(r => Guid.NewGuid()).First().Id)
                    .RuleFor(o=> o.OfficeId, ()=> context.Set<Office>().ToList().OrderBy(r=> Guid.NewGuid()).First().Id)
                    .RuleFor(o => o.DivisionId, () => context.Set<Division>().ToList().OrderBy(r => Guid.NewGuid()).First().Id)
                    .RuleFor(o => o.PositionId, () => context.Set<Position>().ToList().OrderBy(r => Guid.NewGuid()).First().Id);

                var employees = fakeEmployees.Generate(20);
                context.AddRange(employees);
                context.SaveChanges();

                count = 1;
                List<Employee> employeesList = context.Employess.ToList();
                foreach (Employee emp in employeesList)
                {
                    int Year = 2021;
                    DateTime firstDayYear = Convert.ToDateTime("2021/01/01");
                    var salariesYear = context.Salaries.Where(x => x.EmployeeId == emp.Id && x.Year == Year).ToList();
                     int Month = Enumerable.Range(1, 12).Where(i => !salariesYear.Select(m => m.Month).Contains(i)).OrderBy(r => Guid.NewGuid()).First();
                        DateTime YearMonth = Convert.ToDateTime(Year + "/" + Month + "/01");
                        DateTime StartDate = firstDayYear.AddDays(new Random().Next((YearMonth - firstDayYear).Days));


                    var fakeSalaries = new Faker<Salary>()
                            .RuleFor(o => o.EmployeeId, emp.Id)
                            .RuleFor(o => o.Year, Year)                            
                            .RuleFor(o => o.Month, Month)
                            .RuleFor(o => o.BaseSalary, f => Math.Round(f.Random.Double(10000, 80000),2))
                            .RuleFor(o => o.ProductionBonus, f => Math.Round(f.Random.Double(0, 10000),2))
                            .RuleFor(o => o.CompesationBonus, f => Math.Round(f.Random.Double(0, 10000),2))
                            .RuleFor(o => o.Comission, f => Math.Round(f.Random.Double(0, 10000),2))
                            .RuleFor(o => o.Contributions, f => Math.Round(f.Random.Double(0.0, 10000.00),2))
                            .RuleFor(o=>o.StartDate,StartDate);

                    var fakeNewSalary = fakeSalaries.Generate(8).First();
                    context.Salaries.Add(new Salary(
                          fakeNewSalary.EmployeeId,
                          fakeNewSalary.Year,
                          fakeNewSalary.Month,
                          fakeNewSalary.BaseSalary,
                          fakeNewSalary.ProductionBonus,
                          fakeNewSalary.CompesationBonus,
                          fakeNewSalary.Comission,
                          fakeNewSalary.Contributions,fakeNewSalary.StartDate
                        ));
                    context.SaveChanges();

                }
            }
            public void Dispose() => Connection.Dispose();
        }

    }

