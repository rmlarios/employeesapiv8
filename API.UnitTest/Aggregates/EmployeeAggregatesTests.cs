﻿using Domain.Entities.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static API.UnitTest.SqliteInMemoryTest;

namespace API.UnitTest.Aggregates
{
    public class EmployeeAggregatesTests : IClassFixture<SqliteInMemoryTest>
    {
        private SqliteInMemoryTest _fixture { get; }
        public EmployeeAggregatesTests(SqliteInMemoryTest fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void AddUpdateEmployee_DuplicatedFullName_ThrowsException()
        {
            using (var context = _fixture.CreateContext())
            {
                var officeId = context.Offices.FirstOrDefault().Id;
                var divisionId = context.Divisions.FirstOrDefault().Id;
                var positionId = context.Positions.FirstOrDefault().Id;

                //var sample = new Employee("9876543210", "Employee 1", "SurName 1", 25, DateTime.Now, "0123456789",officeId,divisionId,positionId);
                var newEmployee = new Employee();
                //Assert.Throws<Exception>(()=>newEmployee.Update())
            }
        }

    }
}
