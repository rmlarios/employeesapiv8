﻿using Domain.Entities.Employees;
using Domain.Interfaces;
using Infraestructure.Data;
using Infraestructure.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Services.Employees;
using API.Services.Catalogs;
using API.Services.Salaries;
using Domain.Entities.Salarys;

namespace API.Extensions
{
  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection AddRepositories(this IServiceCollection services)
    {
      return services
          .AddScoped(typeof(IAsyncRepository<>), typeof(RepositoryBase<>))
          .AddScoped<IEmployeeRepository, EmployeeRepository>()
          .AddScoped<ISalaryRepository, SalaryRepository>();
    }

    public static IServiceCollection AddUnitOfWork(this IServiceCollection services)
    {
      return services
          .AddScoped<IUnitOfWork, UnitOfWork>();
    }

    public static IServiceCollection AddDatabase(this IServiceCollection services
       , IConfiguration configuration)
    {
      return services.AddDbContext<DBContext>(options => options.UseSqlServer(configuration.GetConnectionString("DBConnectionString")));
    }

    public static IServiceCollection AddBusinessServices(this IServiceCollection services
      )
    {
      return services
          .AddScoped<EmployeeService>()
          .AddScoped<CatalogService>()
          .AddScoped<SalaryService>()
         ;
    }
  }
}
