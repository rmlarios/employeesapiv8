using System;
using System.Threading.Tasks;
using API.Services.Catalogs;
using Domain.Base;
using Domain.Entities.Divisions;
using Domain.Entities.Offices;
using Domain.Entities.Positions;
//using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CatalogController : ControllerBase
  {
    private readonly CatalogService _service;
    private readonly ILogger<CatalogController> _logger;

    public CatalogController(ILogger<CatalogController> logger, CatalogService service)
    {
      _service = service;
      _logger = logger;
    }

    [HttpGet("getOffices")]
    public async Task<IActionResult> GetOffices()
    {
      var offices = await _service.GetList<Office>();
      return Ok(offices);
    }

    [HttpGet("getPositions")]
    public async Task<IActionResult> GetPositions()
    {
      var positions = await _service.GetList<Position>();
      return Ok(positions);
    }

    [HttpGet("getDivisions")]
    public async Task<IActionResult> GetDivision()
    {
      var divisions = await _service.GetList<Division>();
      return Ok(divisions);
    }
  }
}
