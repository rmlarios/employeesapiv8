﻿using API.DTOs.Employees;
using API.Services.Employees;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class EmployeeController : ControllerBase
  {
    private readonly EmployeeService _service;
    private readonly ILogger<EmployeeController> _logger;

    public EmployeeController(ILogger<EmployeeController> logger, EmployeeService service)
    {
      _service = service;
      _logger = logger;
    }

    [HttpGet("GetById/{id}")]
    public async Task<IActionResult> Get([FromRoute] string id)
    {
      var employee = await _service.GetEmployee(id);
      return Ok(employee);
    }

    [HttpGet("GetList")]
    public async Task<IActionResult> Get([FromQuery] GetEmployeesListRequest request)
    {
      var employees = await _service.GetEmployeesList(request);
      return Ok(employees);
    }

    [HttpGet("search")]
    public async Task<IActionResult> Get([FromQuery] GetEmployeeRequest request)
    {
      var employees = await _service.SearchAsync(request);
      return Ok(employees);
    }

    [HttpPost]
    public async Task<IActionResult> Add([FromBody] AddEmployeeRequest request)
    {
      var employess = await _service.AddNewAsync(request);
      return Ok(employess);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Update(Guid id,[FromBody] AddEmployeeRequest request)
    {
      var employee = await _service.UpdateAsync(request);
      return Ok(employee);
    }



  }
}
