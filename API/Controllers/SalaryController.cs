using System;
using System.Threading.Tasks;
using API.DTOs.Employees;
using API.Services.Salaries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class SalaryController : ControllerBase
  {
    private readonly SalaryService _service;
    private readonly ILogger<SalaryController> _logger;
    public SalaryController(ILogger<SalaryController> logger, SalaryService service)
    {
      _service = service;
      _logger = logger;
    }

     [HttpPost]
    public async Task<IActionResult> AddSalary([FromBody] AddSalaryRequest request)
    {
      var salary = await _service.AddEmployeeSalaryAsync(request);
      return Ok(salary);
    }

    [HttpPost("UpdateSalary/{id}")]
    public async Task<IActionResult> UpdateSalary(Guid id,[FromBody] AddSalaryRequest request)
    {
      var salary = await _service.UpdateEmployeeSalaryAsync(request);
      return Ok(salary);

    }



  }
}
