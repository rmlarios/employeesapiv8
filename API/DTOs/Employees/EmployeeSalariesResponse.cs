using System;

namespace API.DTOs.Employees
{
  public class EmployeeSalariesResponse
  {
    public Guid Id { get; set; }
    public Guid EmployeeId { get; set; }
    public int Year { get; set; }
    public int Month { get; set; }
    public double BaseSalary { get; set; }
    public double ProductionBonus { get; set; }
    public double CompesationBonus { get; set; }
    public double Comission { get; set; }
    public double Contributions { get; set; }
    public double OtherIncome { get; set; }
    public double TotalSalary { get; set; }

        public DateTime StartDate { get; set; }

    }
}
