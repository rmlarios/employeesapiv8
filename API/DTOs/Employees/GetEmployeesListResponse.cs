﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entities.Salarys;

namespace API.DTOs.Employees
{
    public class GetEmployeesListResponse
    {
        public Guid Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public string EmployeeCode { get; set; }
        public string IdentificationNumber { get; set; }
        public int Grade { get; set; }
        public DateTime BirthDay { get; set; }
        public Guid OfficeId { get; set; }
        public Guid DivisionId { get; set; }
        public Guid PositionId { get; set; }
        public Double TotalSalary { get; set; } = 0;

    public string DivisionName { get; set; } = "";
    public string PositionName { get; set; } = "";
    public string OfficeName { get; set; } = "";

    public List<EmployeeSalariesResponse> Salaries { get; set; } = null;
    public double Bonus { get; set; } = 0;

  }
}
