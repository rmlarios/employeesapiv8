﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs.Employees
{
  public class GetEmployeesListRequest
  {
    public int? Grade { get; set; } = null;
    public Guid? PositionId { get; set; } = null;
    public Guid? OfficeId { get; set; } = null;
    public int pageIndex { get; set; }
    public int pageSize { get; set; }
  }
}
