﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs.Employees
{
    public class AddSalaryResponse
    {
        public Guid EmployeeId { get; set; }
        public double TotalSalary { get; set; }
        public double  OtherIncome { get; set; }
    }
}
