﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs.Employees
{
    public class DeleteEmployeeRequest
    {
        public Guid Id { get; set; }
    }
}
