﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs.Employees
{
    public class AddEmployeeResponse
    {
        public Guid Id { get; set; }
        public string EmployeeName { get; set; }
    }
}
