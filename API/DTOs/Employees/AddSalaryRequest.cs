﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs.Employees
{
    public class AddSalaryRequest
    {
        [Required]
        public Guid EmployeeId { get; set; }
        [Required]        
        public int Year { get; set; }
        [Required]
        //[Range(1,12)]
        public int Month { get; set; }
        [Required]
        [Range(Double.MinValue,Double.MaxValue)]
        public double BaseSalary { get; set; }
        [Required]
        [Range(0, Double.MaxValue)]
        public double ProductionBonus { get; set; } = 0;
        [Required]
        [Range(0, Double.MaxValue)]
        public double CompesationBonus { get; set; } = 0;
        [Required]
        [Range(0, Double.MaxValue)]
        public double Comission { get; set; } = 0;
        [Required]
        [Range(0, Double.MaxValue)]
        public double Contributions { get; set; } = 0;
        public Guid? Id { get; set; }

        public DateTime StartDate { get; set; }
    }
}
