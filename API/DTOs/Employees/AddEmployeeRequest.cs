﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs.Employees
{
    public class AddEmployeeRequest
    {
        [Required]        
        [StringLength(10, MinimumLength = 10)]
        public string employeeCode { get; set; }
        [Required]
        [StringLength(150)]
        public string employeeName { get; set; }
        [Required]
        [StringLength(150)]
        public string employeeSurname { get; set; }
        [Required]
        [Range(1, 1000)]
        public int grade { get; set; }
        [Required]
        public DateTime birthday { get; set; }
        [Required]
        public Guid officeId { get; set; }
        [Required]
        public Guid divisionId { get; set; }
        [Required]
        public Guid positionId { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 10)]
        public string identificationNumber { get; set; }
        public Guid? id { get; set; } = null;



    }
}
