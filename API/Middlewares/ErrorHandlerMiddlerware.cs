using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using API.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace API.Middlewares
{
  public class ErrorHandlerMiddlerware
  {
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;

    class CustomErrorResponse
    {
     public string Message { get; set; }
     public string StatusCode { get; set; }
     public string Type{ get; set; }
      public bool IsError { get; set; } = true;

    }

    public ErrorHandlerMiddlerware(RequestDelegate next,ILogger<Log> logger)
    {
      _next = next;
      _logger = logger;
    }

    public async Task Invoke(HttpContext context)
    {
      try
      {
        await _next(context);
      }
      catch (Exception error)
      {
        var response = context.Response;
        response.ContentType = "application/json";
        //  var responseModel = new CustomErrorResponse() { Message = error?.Message};  
        var errorResponse = new APIResponse<string>(error?.Message);

        switch (error)
        {           
          
          case ValidationException e:
            // custom application error
            response.StatusCode = (int)HttpStatusCode.BadRequest;
            errorResponse.ExceptionType = "Validation Error";
            //responseModel.Errors = e;
            break;
          case KeyNotFoundException e:
            // not found error
            response.StatusCode = (int)HttpStatusCode.NotFound;
            errorResponse.ExceptionType = "Not Found";
            break;
          default:
            // unhandled error
            response.StatusCode = (int)HttpStatusCode.InternalServerError;
            errorResponse.ExceptionType = "App Error";
            _logger.LogError(error, error?.Message);
            break;
        }
       
        errorResponse.StatusCode = response.StatusCode.ToString();
        var result = JsonSerializer.Serialize(errorResponse);

        await response.WriteAsync(result);
      }
    }
  }
}
