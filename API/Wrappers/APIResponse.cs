using System;
using System.Collections.Generic;
using System.Net;

namespace API.Wrappers
{
  public class APIResponse<T>
  {

    public APIResponse()
    {

    }
    public APIResponse(List<T> datas, int count = 0)
    {
      Datas = datas;
      Count = count != 0 ? count : Datas.Count;
    }

    public APIResponse(T data, string message = null)
    {
      Message = message;
      Data = data;
    }

    public APIResponse(string message, bool isError = true)
    {
      IsError = isError;
      Message = message;
      //StatusCode = statusCode;
     // ExceptionType = errorType;
    }




    public string Message { get; set; } = "";
    public bool IsError { get; set; } = false;
    public string StatusCode { get; set; } = HttpStatusCode.OK.ToString();
    public string ExceptionType { get; set; } = null;
    public T Data { get; set; }
    public List<T> Datas { get; set; }
    public int Count { get; set; } = 0;


  }
}
