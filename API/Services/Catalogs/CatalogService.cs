using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Base;
using Domain.Interfaces;
using API.Wrappers;

namespace API.Services.Catalogs
{
  public class CatalogService : BaseService
  {
    private IAsyncRepository<BaseEntity> _repository;
    public CatalogService(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
      _repository = unitOfWork.AsyncRepository<BaseEntity>();
    }

    public async Task<APIResponse<M>> GetList<M>() where M : class
    {
      return new APIResponse<M>(await _repository.ListAll<M>());
    }
  }
}
