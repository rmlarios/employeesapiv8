using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTOs.Employees;
using API.Wrappers;
using Domain.Entities.Salarys;
using Domain.Interfaces;

namespace API.Services.Salaries
{
  public class SalaryService : BaseService
  {
    private IAsyncRepository<Salary> _repository;

    public SalaryService(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
      _repository = unitOfWork.AsyncRepository<Salary>();
    }


    public async Task<APIResponse<AddSalaryResponse>> AddEmployeeSalaryAsync(AddSalaryRequest model)
    {
      var exist = await _repository.GetAsync(x=>x.EmployeeId==model.EmployeeId && x.Year== model.Year && x.Month==model.Month);
      if (exist == null)
      {
        //var salary = employee.AddSalary(model.Year, model.Month, model.BaseSalary, model.ProductionBonus, model.CompesationBonus, model.Comission, model.Contributions);
                
        var salary = new Salary(model.EmployeeId, model.Year, model.Month, model.BaseSalary, model.ProductionBonus, model.CompesationBonus, model.Comission, model.Contributions,model.StartDate);

        await _repository.AddAsync(salary);
        await UnitOfWork.SaveChangesdAsync();

        return new APIResponse<AddSalaryResponse>(new AddSalaryResponse()
        {
          EmployeeId = model.EmployeeId,
          TotalSalary = salary.TotalSalary,
          OtherIncome = salary.OtherIncome

        });
      }
      throw new Exception("Salary for this Year and Month already exists.");
    }

    public async Task<APIResponse<AddSalaryResponse>> UpdateEmployeeSalaryAsync(AddSalaryRequest model)
    {
      var current = await _repository.GetAsync(x => x.Id == model.Id);
      var exist = await _repository.GetAsync(x=>x.EmployeeId==model.EmployeeId && x.Year== model.Year && x.Month==model.Month && x.Id!=model.Id);
      if(exist!=null)
        throw new Exception("Salary for this Year and Month already exists.");

      if (current != null)
      {        

        current.Update(model.EmployeeId, model.Year, model.Month, model.BaseSalary, model.ProductionBonus, model.CompesationBonus, model.Comission, model.Contributions,model.StartDate);

        await _repository.UpdateAsync(current);
        await UnitOfWork.SaveChangesdAsync();

        return new APIResponse<AddSalaryResponse>(new AddSalaryResponse()
        {
          EmployeeId = current.EmployeeId,
          TotalSalary = current.TotalSalary,
          OtherIncome = current.OtherIncome

        });
      }

      throw new KeyNotFoundException("Salary record Not Found");
    }

  }
}
