﻿using API.DTOs.Employees;
using API.Wrappers;
using Domain.Entities.Employees;
using Domain.Entities.Salarys;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API.Services.Employees
{
  public class EmployeeService : BaseService
  {
    private IAsyncRepository<Employee> _repository;
    public EmployeeService(IUnitOfWork unitOfWork) : base(unitOfWork)
    {
      _repository = UnitOfWork.AsyncRepository<Employee>();
    }

    public async Task<APIResponse<GetEmployeesListResponse>> GetEmployee(string id)
    {
      var employee = await _repository.GetAsync(x => x.Id.ToString() == id, "Salarys,Division,Position,Office");
      if (employee != null)
        return new APIResponse<GetEmployeesListResponse>(new GetEmployeesListResponse()
        {
          Id = employee.Id,
          BirthDay = employee.Bithday,
          EmployeeName = employee.EmployeeName,
          EmployeeSurname = employee.EmployeeSurname,
          EmployeeCode = employee.EmployeeCode,
          IdentificationNumber = employee.IdentificationNumber,
          Grade = employee.Grade,
          DivisionId = employee.DivisionId,
          DivisionName = employee.Division.Name,
          PositionId = employee.PositionId,
          PositionName = employee.Position.Name,
          OfficeId = employee.OfficeId,
          OfficeName = employee.Office.Name,
          TotalSalary = employee.Salarys.OrderByDescending(o => o.Year).ThenByDescending(o => o.Month).Select(a => a.TotalSalary).DefaultIfEmpty(0).FirstOrDefault(),
          Salaries = employee.Salarys.Select(e => new EmployeeSalariesResponse()
          {
            Id = e.Id,
            EmployeeId = e.EmployeeId,
            Year = e.Year,
            Month = e.Month,
            BaseSalary = e.BaseSalary,
            ProductionBonus = e.ProductionBonus,
            CompesationBonus = e.CompesationBonus,
            Comission = e.Comission,
            Contributions = e.Contributions,
            OtherIncome = e.OtherIncome,
            TotalSalary = e.TotalSalary,
            StartDate= e.StartDate
          }).ToList()
        });

      throw new KeyNotFoundException("Employee not exists.");
    }

    public async Task<APIResponse<AddEmployeeResponse>> AddNewAsync(AddEmployeeRequest model)
    {
      var employee = new Employee(model.employeeCode, model.employeeName, model.employeeSurname, model.grade, model.birthday, model.identificationNumber, model.officeId, model.divisionId, model.positionId);
      var exist = await _repository.GetAsync(x => x.EmployeeName == employee.EmployeeName && x.EmployeeSurname == employee.EmployeeSurname);
      if (exist == null)
      {
        //var repository = UnitOfWork.AsyncRepository<Employee>();
        await _repository.AddAsync(employee);
        await UnitOfWork.SaveChangesdAsync();

        var response = new AddEmployeeResponse()
        {
          Id = employee.Id,
          EmployeeName = employee.EmployeeName + ' ' + employee.EmployeeSurname
        };
        return new APIResponse<AddEmployeeResponse>(response);

      }
      throw new Exception("Employee already exists.");
    }

    public async Task<APIResponse<AddEmployeeResponse>> UpdateAsync(AddEmployeeRequest model)
    {
      //var employee = new Employee(model.EmployeeCode, model.EmployeeName, model.EmployeeSurname, model.Grade, model.Bithday, model.IdentificationNumber, model.OfficeId, model.DivisionId, model.PositionId);

      var current = await _repository.GetAsync(x => x.Id == model.id);
      var existSameName = await _repository.GetAsync(x => x.EmployeeName == model.employeeName && x.EmployeeSurname == model.employeeSurname && x.Id != model.id);

      if (existSameName != null) throw new Exception("Employee already exists.");

      if (current != null)
      {
        current.Update(model.employeeCode, model.employeeName, model.employeeSurname, model.grade, model.birthday, model.identificationNumber, model.officeId, model.divisionId, model.positionId);
        await _repository.UpdateAsync(current);
        await UnitOfWork.SaveChangesdAsync();

        var response = new AddEmployeeResponse()
        {
          Id = current.Id,
          EmployeeName = current.EmployeeName + ' ' + current.EmployeeSurname
        };
        return new APIResponse<AddEmployeeResponse>(response);

      }
      throw new Exception("Employee not exists.");
    }

 
    public async Task<APIResponse<GetEmployeesListResponse>> SearchAsync(GetEmployeeRequest request)
    {
      var e = await _repository.GetAsync(x => x.EmployeeCode == request.code, "Salarys");

      if (e == null)
        throw new Exception("Employee not exists.");

      var employeeDTO = new GetEmployeesListResponse()
      {
        Id = e.Id,
        BirthDay = e.Bithday,
        EmployeeName = e.EmployeeName,
        EmployeeSurname = e.EmployeeSurname,
        EmployeeCode = e.EmployeeCode,
        IdentificationNumber = e.IdentificationNumber,
        Grade = e.Grade,
        DivisionId = e.DivisionId,
        PositionId = e.PositionId,
        OfficeId = e.OfficeId,
        TotalSalary = 0
      };

      if (e.Salarys.Count == 0)
        return new APIResponse<GetEmployeesListResponse>(employeeDTO);

      var LastStartDate = e.Salarys.OrderByDescending(o => o.StartDate).First().StartDate;
      var previousDate = e.Salarys.Where(f => f.StartDate != LastStartDate).OrderByDescending(o => o.StartDate).Skip(1).First().StartDate;

      var lastSalaries = e.Salarys.Where(s => s.StartDate > previousDate && s.StartDate <= LastStartDate).Select(
      e =>
      new
      {
        Total = e.TotalSalary,
        YearMonth = Convert.ToDateTime(e.Year + "/" + e.Month + "/01"),
        SalaryId = e.Id
      }
      )
      .ToList();

      if (lastSalaries.Count > 0)
      {
        Guid? s1ID = null, s2ID = null, s3ID = null;//ID de los ultimos 3 salarios a incluir en el response
        double bonus = 0;

        //Get 3 consecutive salaries
        var consecutive_3 = from f1 in lastSalaries
                            from f2 in lastSalaries
                            from f3 in lastSalaries
                            let salary3 = f3.SalaryId
                            let salary2 = f2.SalaryId
                            let salary1 = f1.SalaryId
                            let Bonus = (f1.Total + f2.Total + f3.Total) / 3
                            where f1.YearMonth.AddMonths(1) == f2.YearMonth && f2.YearMonth.AddMonths(1) == f3.YearMonth
                            select new
                            {
                              s3 = salary3,
                              s2 = salary2,
                              s1 = salary1,
                              B = Bonus
                            };
        if (consecutive_3.FirstOrDefault() != null)
        {
          s1ID = consecutive_3.First().s1;
          s2ID = consecutive_3.First().s2;
          s3ID = consecutive_3.First().s3;
          bonus = consecutive_3.First().B;
        }
        else //SI NO HA 3 Consecutivos buscamos al menos 2
        {
          var consecutive_2 = from f1 in lastSalaries
                              from f2 in lastSalaries
                              let salary2 = f2.SalaryId
                              let salary1 = f1.SalaryId
                              let Bonus = (f1.Total + f2.Total) / 3
                              where f1.YearMonth.AddMonths(1) == f2.YearMonth
                              select new
                              {
                                s2 = salary2,
                                s1 = salary1,
                                B = Bonus
                              };
          if (consecutive_2.FirstOrDefault() != null)
          {
            s1ID = consecutive_2.First().s1;
            s2ID = consecutive_2.First().s2;
            bonus = consecutive_2.First().B;
          }
          else
          {
            s1ID = lastSalaries.OrderByDescending(x => x.YearMonth).First().SalaryId;
            bonus = lastSalaries.OrderByDescending(x => x.YearMonth).First().Total / 3;
          }
        }

        employeeDTO.Salaries = e.Salarys.Where(x => x.Id == s1ID || x.Id == s2ID || x.Id == s3ID)
        .Select(e => new EmployeeSalariesResponse()
        {
          Id = e.Id,
          EmployeeId = e.EmployeeId,
          Year = e.Year,
          Month = e.Month,
          BaseSalary = e.BaseSalary,
          ProductionBonus = e.ProductionBonus,
          CompesationBonus = e.CompesationBonus,
          Comission = e.Comission,
          Contributions = e.Contributions,
          OtherIncome = e.OtherIncome,
          TotalSalary = e.TotalSalary
        }).ToList();
        employeeDTO.Bonus = bonus;
      }

      return new APIResponse<GetEmployeesListResponse>(employeeDTO);

    }
  
     public async Task<APIResponse<GetEmployeesListResponse>> GetEmployeesList(GetEmployeesListRequest request)
    {
      Expression<Func<Employee, bool>> exp = a => a.Id != null;

      if (request.OfficeId != null && request.Grade != null)
        exp = a => a.OfficeId == request.OfficeId && a.Grade == request.Grade;
      else if (request.OfficeId == null && request.Grade != null && request.PositionId == null)
        exp = a => a.Grade == request.Grade;
      else if (request.PositionId != null && request.Grade != null)
        exp = a => a.PositionId == request.PositionId && a.Grade == request.Grade;


      var employees = await _repository.ListAsync(exp, "Salarys,Division,Position,Office",request.pageIndex,request.pageSize);
      var employeeDTOs = employees.Select(e => new GetEmployeesListResponse()
      {
        Id = e.Id,
        BirthDay = e.Bithday,
        EmployeeName = e.EmployeeName,
        EmployeeSurname = e.EmployeeSurname,
        EmployeeCode = e.EmployeeCode,
        IdentificationNumber = e.IdentificationNumber,
        Grade = e.Grade,
        DivisionId = e.DivisionId,
        DivisionName = e.Division.Name,
        PositionId = e.PositionId,
        PositionName = e.Position.Name,
        OfficeId = e.OfficeId,
        OfficeName = e.Office.Name,
        TotalSalary = e.Salarys.OrderByDescending(o => o.Year).ThenByDescending(o => o.Month).Select(a => a.TotalSalary).DefaultIfEmpty(0).FirstOrDefault()
      }).ToList();
      return new APIResponse<GetEmployeesListResponse>(employeeDTOs,await _repository.GetCount<Employee>(exp));
    }


  
  }
}
