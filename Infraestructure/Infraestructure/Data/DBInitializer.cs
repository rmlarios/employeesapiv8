﻿using Bogus;
using Domain.Entities.Divisions;
using Domain.Entities.Employees;
using Domain.Entities.Offices;
using Domain.Entities.Positions;
using Domain.Entities.Salarys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infraestructure.Data
{
    public static class DBInitializer
    {
        public static void Initialize(DBContext context)
        {
            context.Database.EnsureCreated();

            //Seed Offices Data
            if (!context.Offices.Any())
            {
                var offices = new Office[]
                {
                    new Office(name:"A"),
                    new Office(name:"B"),
                    new Office(name:"C"),
                    new Office(name:"D"),
                    new Office(name:"E"),
                    new Office(name:"F"),
                    new Office(name:"G"),
                    new Office(name:"H"),
                    new Office(name:"I"),
                    new Office(name:"J"),
                    new Office(name:"K"),
                    new Office(name:"L"),
                    new Office(name:"M"),
                    new Office(name:"N")
                };
                foreach (Office o in offices)
                {
                    context.Offices.Add(o);
                }
                context.SaveChanges();
            }

            //Seed Division Data
            if (!context.Divisions.Any())
            {
                var divisions = new Division[]
                {
                    new Division("Operation"),
                    new Division("Sales"),
                    new Division("Marketing"),
                    new Division("Customer Care")
                };

                foreach (Division d in divisions)
                {
                    context.Divisions.Add(d);
                }
                context.SaveChanges();

            }

            //Seed Position Data
            if (!context.Positions.Any())
            {
                var positions = new Position[]
                {
                    new Position("Operation"),
                    new Position("Cargo Manager"),
                    new Position("Head of Cargo"),
                    new Position("Cargo Assistant"),
                    new Position("Account Executive"),
                    new Position("Marketing Assistant"),
                    new Position("Customer Director")
                };

                foreach (Position p in positions)
                {
                    context.Positions.Add(p);
                }
                context.SaveChanges();

            }

            //Seed Employees Data
            if (!context.Employess.Any())
            {
                DateTime startDate = new DateTime(1970, 1, 1);
                for (int i = 1; i <= 100; i++)
                {

                    context.Employess.Add(
                        new Employee(
                            code: new Random().Next(1000, 1100).ToString() + new Random().Next(1, 100).ToString("D3") + DateTime.Now.Millisecond.ToString("D3"),
                            name: "Employee " + i.ToString(),
                            surname: "Surname " + i.ToString(),
                            grade: new Random().Next(1, 20),
                            birthdate: startDate.AddDays(new Random().Next((DateTime.Today - startDate).Days)),
                            identificationNumber: new Random().Next().ToString("D10"),
                            officeId: context.Offices.OrderBy(r => Guid.NewGuid()).First().Id,
                            divisionId: context.Divisions.OrderBy(r => Guid.NewGuid()).First().Id,
                            positionId: context.Positions.OrderBy(r => Guid.NewGuid()).First().Id
                            )
                        );
                    context.SaveChanges();
                }
            }

            //Seed Salaries Data
            if (!context.Salaries.Any())
            {
                //var salary = employee.AddSalary(model.Year, model.Month, model.BaseSalary, model.ProductionBonus, model.CompesationBonus, model.Comission, 

                List<Employee> employeesList = context.Employess.ToList();

                foreach (Employee emp in employeesList)
                {                                    

                    for (int i = 1; i <= 6; i++)
                    {
                        int Year = 2021;
                        DateTime firstDayYear = Convert.ToDateTime("2021/01/01");
                        var salariesYear = context.Salaries.Where(x => x.EmployeeId == emp.Id && x.Year == Year).ToList();
                        int Month = Enumerable.Range(1, 12).Where(i => !salariesYear.Select(m => m.Month).Contains(i)).OrderBy(r => Guid.NewGuid()).First();
                        DateTime YearMonth = Convert.ToDateTime(Year + "/" + Month + "/01");
                        DateTime StartDate = firstDayYear.AddDays(new Random().Next((YearMonth - firstDayYear).Days));

                        var fakeSalary = new Faker<Salary>()
                            //.RuleFor(o => o.EmployeeId, () => context.Employess.OrderBy(r => Guid.NewGuid()).First().Id)
                            .RuleFor(o => o.EmployeeId, emp.Id)
                            .RuleFor(o => o.Year, Year)
                            //.RuleFor(o => o.Month, f => f.Random.Int(1, 12))
                            .RuleFor(o => o.Month, Month)
                            .RuleFor(o => o.BaseSalary, f => f.Random.Double(10000, 80000))
                            .RuleFor(o=> o.StartDate, StartDate)
                            .RuleFor(o => o.ProductionBonus, f => f.Random.Double(0, 10000))
                            .RuleFor(o => o.CompesationBonus, f => f.Random.Double(0, 10000))
                            .RuleFor(o => o.Comission, f => f.Random.Double(0, 10000))
                            .RuleFor(o => o.Contributions, f => f.Random.Double(0.0, 10000.00));

                        var fakeNewSalary = fakeSalary.Generate(1).First();
                        context.Salaries.Add(new Salary(
                              fakeNewSalary.EmployeeId,
                              fakeNewSalary.Year,
                              fakeNewSalary.Month,
                              fakeNewSalary.BaseSalary,
                              fakeNewSalary.ProductionBonus,
                              fakeNewSalary.CompesationBonus,
                              fakeNewSalary.Comission,
                              fakeNewSalary.Contributions,fakeNewSalary.StartDate
                            )
                        { StartDate = fakeNewSalary.StartDate }); ;
                        context.SaveChanges();
                    }
                }

                /*var fakeSalaries = new Faker<Salary>()
                            .RuleFor(o=> o.EmployeeId, () => context.Employess.OrderBy(r => Guid.NewGuid()).First().Id)
                            .RuleFor(o=>o.Year,f=>f.Random.Int(2010,2021))                            
                            .RuleFor(o=>o.Month,f=>f.Random.Int(1,12))
                            .RuleFor(o => o.BaseSalary,f => f.Random.Double(10000,80000))
                            .RuleFor(o=> o.ProductionBonus,f=>f.Random.Double(0,10000))
                            .RuleFor(o=> o.CompesationBonus,f=>f.Random.Double(0,10000))
                            .RuleFor(o=> o.Comission,f=>f.Random.Double(0,10000));
                */

               

            }
        }

    }
}
