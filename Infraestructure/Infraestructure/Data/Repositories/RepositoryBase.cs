﻿using Domain.Base;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data.Repositories
{
  public class RepositoryBase<T> : IAsyncRepository<T> where T : BaseEntity
  {
    private readonly DbSet<T> _dbSet;
    private readonly DBContext _context;

    public RepositoryBase(DBContext dBContext)
    {
      _dbSet = dBContext.Set<T>();
      _context = dBContext;
    }

    public async Task<T> AddAsync(T entity)
    {
      await _dbSet.AddAsync(entity);
      return entity;
    }

    public Task<bool> DeleteAsync(T entity)
    {
      _dbSet.Remove(entity);
      return Task.FromResult(true);
    }

    public Task<T> GetAsync(Expression<Func<T, bool>> expression, string IncludeProperties="")
    {
      IQueryable<T> query = _dbSet;
      if(IncludeProperties!="")
      {
        foreach (var includeProperty in IncludeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        {
          query = query.Include(includeProperty);
        }
      }
      return query.Where(expression).FirstOrDefaultAsync();

      //var query = _dbSet.FirstOrDefaultAsync(expression);
    }

    public async Task<int> GetCount<M>(Expression<Func<M, bool>> expression = null) where M : class
    {
      if (expression == null)
        return await _context.Set<M>().CountAsync();
      else
        return await _context.Set<M>().Where(expression).CountAsync();
    }

    public Task<List<M>> ListAll<M>() where M : class
    {
      return _context.Set<M>().ToListAsync();
    }

    public Task<List<T>> ListAsync(Expression<Func<T, bool>> expression=null, string IncludeProperties = "",int pageIndex=0,int pageSize=0)
    {
      IQueryable<T> query = _dbSet;
      if(expression!=null)
        query = _dbSet.Where(expression);//.Include(IncludeProp,).ToListAsync();
        
      if (IncludeProperties != "")
      {
        foreach (var includeProperty in IncludeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        {
          query = query.Include(includeProperty);
        }
      }
      
      if(pageIndex==0 && pageSize==0)
        return query.ToListAsync();
      else
      {
        return query.Skip((pageIndex-1)*pageSize).Take(pageSize).ToListAsync();
      }

    }


    public Task<T> UpdateAsync(T entity)
    {
      _dbSet.Update(entity);
      return Task.FromResult(entity);
    }
  }
}

