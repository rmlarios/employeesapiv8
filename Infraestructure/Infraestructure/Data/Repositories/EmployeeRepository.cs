﻿using Domain.Entities.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructure.Data.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>,IEmployeeRepository
    {
        public EmployeeRepository(DBContext dBContext): base(dBContext)
        {

        }
    }
}
