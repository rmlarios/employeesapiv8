using System;
using Domain.Entities.Salarys;

namespace Infraestructure.Data.Repositories
{
  public class SalaryRepository : RepositoryBase<Salary>, ISalaryRepository
  {
    public SalaryRepository(DBContext dBContext) : base(dBContext)
    {
    }
  }
}
