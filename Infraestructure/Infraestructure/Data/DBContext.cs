﻿using Domain.Base;
using Domain.Entities.Divisions;
using Domain.Entities.Employees;
using Domain.Entities.Offices;
using Domain.Entities.Positions;
using Domain.Entities.Salarys;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infraestructure.Data
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {

        }

        public DbSet<Employee> Employess { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Position> Positions { get; set; }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = new CancellationToken()){
            var selectedEntityList = ChangeTracker.Entries()  
                                    .Where(x => x.Entity is BaseEntity &&  
                                    (x.State == EntityState.Added || x.State == EntityState.Modified));  
  
            
  
            foreach (var entity in selectedEntityList)  
            {  
  
                ((BaseEntity)entity.Entity).LastModifiedOn = DateTime.Now;  
                //((BaseEntity)entity.Entity).ModifiedUserName = userName;  
            }  
  
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);  

        }



    }
}
