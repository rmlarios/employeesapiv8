﻿using Domain.Base;
using Domain.Interfaces;
using Infraestructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data
{
    public class UnitOfWork:IUnitOfWork
    {
        private readonly DBContext _dBContext;

        public UnitOfWork(DBContext dBContext)
        {
            _dBContext = dBContext;

        }

        public IAsyncRepository<T> AsyncRepository<T>() where T : BaseEntity
        {
            return new RepositoryBase<T>(_dBContext);
        }

        public Task<int> SaveChangesdAsync()
        {            
            return _dBContext.SaveChangesAsync();
        }
    }
}
